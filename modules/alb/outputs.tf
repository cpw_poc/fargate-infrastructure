output "arn" {
  value = aws_alb.this.arn
}


output "security_group_id" {
  value = aws_security_group.this.id
}

output "listener_arn" {
  value = aws_lb_listener.http.arn
}

