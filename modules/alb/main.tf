locals {
  alb_name     = "${var.name}-alb"
  s3_logs_name = "${var.name}-alb-logs-11223344465"

  tags = merge(var.tags, {
    TfModule = "alb"
  })
}

data "aws_elb_service_account" "this" {}

resource "aws_s3_bucket" "logs" {
  bucket = local.s3_logs_name

  lifecycle {
    prevent_destroy = false
  }

  force_destroy = true

  tags = local.tags
}

resource "aws_s3_bucket_policy" "logs" {
  bucket = aws_s3_bucket.logs.id
    policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "s3:PutObject"
        Principal = {
          AWS = data.aws_elb_service_account.this.arn
        }
        Effect   = "Allow"
        Resource = ["arn:aws:s3:::${local.s3_logs_name}/*"]
      }
    ]
  })
}

resource "aws_s3_bucket_ownership_controls" "logs" {
  bucket = aws_s3_bucket.logs.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "logs" {
  depends_on = [aws_s3_bucket_ownership_controls.logs]

  bucket = aws_s3_bucket.logs.id
  acl    = "private"
}


resource "aws_s3_bucket_public_access_block" "logs" {
  bucket = aws_s3_bucket.logs.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_security_group" "this" {
  name_prefix = "${var.name}-alb"
  vpc_id      = var.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.tags

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_alb" "this" {
  name            = local.alb_name
  subnets         = var.subnet_ids
  security_groups = [aws_security_group.this.id]

  load_balancer_type = "application"

  access_logs {
    bucket  = aws_s3_bucket.logs.bucket
    prefix  = "${var.name}-alb"
    enabled = true
  }

  tags = local.tags
}

resource "aws_alb_target_group" "this" {
  name        = substr("${var.name}-default", 0, 32)
  vpc_id      = var.vpc_id
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"

  health_check {
    path                = var.health_check_path
    matcher             = "200"
    interval            = 45
    timeout             = 30
    healthy_threshold   = 2
    unhealthy_threshold = 3
  }

  tags = local.tags
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_alb.this.id
  port              = 80
  protocol          = "HTTP"

  default_action {

    type             = "forward"
    target_group_arn = aws_alb_target_group.this.arn
  }
}


