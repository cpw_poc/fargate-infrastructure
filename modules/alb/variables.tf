variable "name" {}

variable "region" {}


variable "vpc_id" {}

variable "subnet_ids" {
  type = list(string)
}

variable "health_check_path" {}

variable "tags" {
  type = map(string)
}

