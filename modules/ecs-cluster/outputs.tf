output "name" {
  value      = aws_ecs_cluster.this.name
  depends_on = [aws_ecs_cluster.this]
}

