locals {
  tags = merge(var.tags, {
    TfModule = "ecs-cluster",
  })
}

resource "null_resource" "ecs-settings" {
  for_each = {
    awsvpcTrunking                 = "enabled",
    containerInstanceLongArnFormat = "enabled",
    serviceLongArnFormat           = "enabled",
    taskLongArnFormat              = "enabled",
  }

  provisioner "local-exec" {
    command = <<EOF
    aws ecs put-account-setting --name ${each.key} --value ${each.value} --region ${var.region}
EOF
  }
}

resource "aws_ecs_cluster" "this" {
  name = var.name
  tags = local.tags

  depends_on = [null_resource.ecs-settings]
}
