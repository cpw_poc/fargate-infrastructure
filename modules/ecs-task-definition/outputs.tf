output "task_definition_arn" {
  value = var.enabled ? aws_ecs_task_definition.this[0].arn : null
}
