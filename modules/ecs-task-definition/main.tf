locals {
  tags = merge(var.tags, {
    TfModule = "ecs-task-definition",
  })
}

resource "aws_cloudwatch_log_group" "this" {
  count             = var.enabled ? 1 : 0
  name              = "/ecs/task/${var.name}"
  retention_in_days = 365
  tags              = local.tags
}

resource "aws_ecs_task_definition" "this" {
  count = var.enabled ? 1 : 0

  family             = var.name
  network_mode       = var.network_mode
  requires_compatibilities = ["FARGATE"]
  execution_role_arn = var.execution_role_arn
  task_role_arn      = var.task_role_arn
  cpu                = var.cpu
  memory = var.memory

  container_definitions = jsonencode([
    for k, container in var.containers : {
      name              = var.name,
      image             = "${container.image_url}:${container.image_tag}",
      command           = container.command,
      memoryReservation = container.memory,
      linuxParameters = container.sharedMemory != null ? {
        sharedMemorySize = container.sharedMemory,
      } : null,
      portMappings = [for p in container.ports :
        {
          containerPort = p,
          hostPort      = p,
        }
      ],
      environment = [
        for k, v in tomap(container.envs) : {
          name  = k,
          value = v
        }
      ]
      logConfiguration = {
        logDriver = "awslogs",
        options = {
          "awslogs-group"         = aws_cloudwatch_log_group.this[0].name
          "awslogs-region"        = var.region
          "awslogs-stream-prefix" = container.name,
        },
      },
      mountPoints = [for volume_def in container.volumes :
        {
          sourceVolume  = volume_def.name,
          containerPath = volume_def.path
        }
      ],
    }
  ])

  dynamic "volume" {
    for_each = var.shared_volumes
    content {
      name = volume.value
      docker_volume_configuration {
        scope         = "shared"
        autoprovision = true
        driver        = "local"
      }
    }
  }

  dynamic "placement_constraints" {
    for_each = var.placement_constraints
    content {
      type       = "memberOf"
      expression = "attribute:${placement_constraints.key} == ${placement_constraints.value}"
    }
  }

  tags = local.tags
}

resource "null_resource" "this" {
  count = var.enabled && var.run_task_immediately_config.enabled ? 1 : 0

  triggers = {
    td = aws_ecs_task_definition.this[0].id
    lg = aws_cloudwatch_log_group.this[0].name
  }

  provisioner "local-exec" {
    command = <<EOF
      aws ecs run-task \
        --cluster ${var.run_task_immediately_config.ecs_cluster} \
        --task-definition ${aws_ecs_task_definition.this[0].id} \
        --launch-type EC2 \
        --region "${var.region}" \
        --count 1
EOF
  }
}
