variable "enabled" {
  type    = bool
  default = true
}

variable "cpu" {
  type = number
}

variable "memory" {
  type = number
}

variable "name" {
  type = string
}

variable "tags" {
  type = map(string)
}

variable "region" {
  type = string
}

variable "placement_constraints" {
  description = "Used only when placement strategy is REPLICA"
  type        = map(string)
  default     = {}
}

variable "network_mode" {
  type = string
}

variable "execution_role_arn" {
  type    = string
  default = ""
}

variable "task_role_arn" {
  type    = string
  default = ""
}

variable "containers" {
  type = list(object({
    name         = string
    image_url    = string
    image_tag    = string
    envs         = map(string)
    ports        = list(number)
    memory       = number
    command      = list(string)
    sharedMemory = number
    volumes = list(object({
      name = string
      path = string
    }))
  }))
}

variable "run_task_immediately_config" {
  type = object({
    enabled     = bool
    ecs_cluster = string
  })
  default = {
    enabled     = false
    ecs_cluster = null
  }
}

variable "shared_volumes" {
  type    = list(string)
  default = []
}
