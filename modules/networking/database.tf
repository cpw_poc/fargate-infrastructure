locals {
  db_subnets_length = length(var.db_subnets)
}

resource "aws_subnet" "db" {
  count = local.db_subnets_length

  vpc_id            = aws_vpc.this.id
  cidr_block        = var.db_subnets[count.index].cidr
  availability_zone = var.db_subnets[count.index].az

  tags = merge(local.tags, {
    Name = "${var.name}-subnet-db${count.index +1}-${var.db_subnets[count.index].az}"
  })
}

# resource "aws_internet_gateway" "public" {
#   vpc_id = aws_vpc.this.id

#   tags = merge(local.tags, {
#     Name = "${var.name}-public"
#   })
# }

# resource "aws_route_table" "public" {
#   vpc_id = aws_vpc.this.id

#   tags = merge(local.tags, {
#     Name = "${var.name}-public"
#   })
# }

# resource "aws_route" "public" {
#   route_table_id         = aws_route_table.public.id
#   destination_cidr_block = "0.0.0.0/0"
#   gateway_id             = aws_internet_gateway.public.id
# }

# resource "aws_route_table_association" "public" {
#   count = local.public_subnets_length

#   route_table_id = aws_route_table.public.id
#   subnet_id      = aws_subnet.public[count.index].id
# }
