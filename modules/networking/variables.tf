variable "name" {
  type = string
}

variable "vpc_cidr" {
  type = string
}

variable "tags" {
  type = map(string)
}

variable "public_subnets" {
  type = list(object({
    cidr = string,
    az   = string,
  }))
}

variable "db_subnets" {
  type = list(object({
    cidr = string,
    az   = string,
  }))
}

variable "private_subnets" {
  type = list(object({
    cidr = string,
    az   = string,
  }))
}

variable "single_nat_gw" {
  description = "Setting this option to true will result in lower cost but no real HA"
  type        = bool
  default     = true
}

# variable "etl_vpc_peering_route" {
#   default = null
#   type = object({
#     destination = string
#     target      = string
#   })
# }

# variable "lpm_vpc_peering_route" {
#   default = null
#   type = object({
#     target      = string
#     destination = string
#   })
# }

