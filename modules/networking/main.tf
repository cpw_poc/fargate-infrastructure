locals {
  tags = merge(var.tags, {
    TfModule = "networking"
  })
}

resource "aws_vpc" "this" {
  cidr_block = var.vpc_cidr

  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(local.tags, {
    Name = "${var.name}-vpc"
  })
}
