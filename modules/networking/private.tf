locals {
  private_subnet_length = length(var.private_subnets)
}

resource "aws_eip" "nat" {
  count = length(var.private_subnets)

  # vpc = true
  domain = "vpc"
  tags = merge(local.tags, {
    "Name" = "${var.name}-nat-gateway-${count.index}"
  })
}

resource "aws_subnet" "private" {
  count = length(var.private_subnets)

  vpc_id            = aws_vpc.this.id
  cidr_block        = var.private_subnets[count.index].cidr
  availability_zone = var.private_subnets[count.index].az

  tags = merge(local.tags, {
    "Name" = "${var.name}-subnet-private${count.index +1}-${var.private_subnets[count.index].az}",
  })
}

resource "aws_nat_gateway" "private" {
  count = var.single_nat_gw ? 1 : length(var.private_subnets)

  allocation_id = aws_eip.nat[count.index].id
  subnet_id     = aws_subnet.public[count.index].id

  tags = merge(local.tags, {
    Name = "${var.name}-nat-${aws_subnet.public[count.index].tags["Name"]}-${count.index +1}"
  })
}

resource "aws_route_table" "private" {
  count = local.private_subnet_length

  vpc_id = aws_vpc.this.id

  tags = merge(local.tags, {
    Name = "${var.name}-rtb-private${count.index +1}-${var.private_subnets[count.index].az}"
  })
}

resource "aws_route" "private" {
  count = local.private_subnet_length

  route_table_id         = aws_route_table.private[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.private, count.index).id
}

# resource "aws_route" "private-2" {
#   count = local.private_subnet_length

#   route_table_id            = aws_route_table.private[count.index].id
#   destination_cidr_block    = var.lpm_vpc_peering_route.destination
#   vpc_peering_connection_id = var.lpm_vpc_peering_route.target
# }

# resource "aws_route" "custom" {
#   count = var.etl_vpc_peering_route == null ? 0 : local.private_subnet_length

#   route_table_id            = aws_route_table.private[count.index].id
#   destination_cidr_block    = var.etl_vpc_peering_route.destination
#   vpc_peering_connection_id = var.etl_vpc_peering_route.target
# }

resource "aws_route_table_association" "private" {
  count = local.private_subnet_length

  route_table_id = aws_route_table.private[count.index].id
  subnet_id      = aws_subnet.private[count.index].id
}
