locals {
  database_subnets = [
    {
      cidr = cidrsubnet(local.vpc_cidr, local.subnet_newbits, 7),
      az   = "${var.region}a",
    },
    {
      cidr = cidrsubnet(local.vpc_cidr, local.subnet_newbits, 8),
      az   = "${var.region}b",
    }
  ]
}

module "postgres" {
  source = "../postgres"

  name = local.stack_name

  apply_immediately = true

  subnets  = local.database_subnets
  vpc_id   = module.networking.vpc_id
  az       = local.database_subnets[0].az
  multi_az = true

  username = var.db_username
  password = var.db_password
  deletion_protection = true
  engine_version         = "12.12"
  parameter_group_family = "postgres12"
  instance_class         = "db.t3.micro"
  storage_type           = "gp3"
  allocated_storage      = 20

  ingress_allow_security_groups = flatten([
   module.communication_tracker.security_group_id,
  #   module.celery.security_group_id,
  #   data.aws_security_group.etl.id,
  #   module.ecs_cluster.ec2_security_groups,
   ])


  subnet_ids = module.networking.db_subnets

  common_tags = local.common_tags
}
