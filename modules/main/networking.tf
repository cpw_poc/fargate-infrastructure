locals {
  vpc_cidr       = "10.0.0.0/16"
  subnet_newbits = 8

  private_subnets = [
    {
      cidr = "10.0.128.0/20",
      az   = "${var.region}a",
    },
    {
      cidr = "10.0.144.0/20",
      az   = "${var.region}b",
    },
  ]

  public_subnets = [
    {
      cidr = "10.0.0.0/20",
      az   = "${var.region}a",
    },
    {
      cidr = "10.0.16.0/20",
      az   = "${var.region}b",
    },
  ]

  db_subnets = [
    {
      cidr = "10.0.50.0/24",
      az   = "${var.region}a",
    },
    {
      cidr = "10.0.60.0/24",
      az   = "${var.region}b",
    },
  ]
}

module "networking" {
  source = "../networking"

  name = local.stack_name

  vpc_cidr        = local.vpc_cidr
  private_subnets = local.private_subnets
  public_subnets  = local.public_subnets
  db_subnets      = local.db_subnets
  # etl_vpc_peering_route = var.etl_vpc_peering_route
  # lpm_vpc_peering_route = var.lpm_vpc_peering_route

  tags = local.common_tags
}


module "alb" {
  source = "../alb"

  vpc_id =  module.networking.vpc_id
  health_check_path = "/"
  region            = var.region
  name              = local.stack_name
  subnet_ids        = module.networking.public_subnets


  tags = local.common_tags
}
