locals {

  communication_tracker_port = 80

  communication_tracker_task_network_mode = "awsvpc"

  communication_tracker_envs = {}


}


module "ecs_cluster" {
  source = "../ecs-cluster"

  name = local.stack_name

  region = var.region


  tags = merge(local.common_tags, {
    Name = "${local.stack_name}-ecs"
  })
}

module "ecs_task_role" {
  source         = "../ecs-task-role"
  name           = local.stack_name
  s3_bucket_name = "aws_s3_bucket.static.id"

  tags = merge(local.common_tags, {
    Name = "${local.stack_name}-ecs-task-role"
  })
}

module "communication_tracker_ecs_task_definition" {
  source = "../ecs-task-definition"

  name = "${local.stack_name}-communication_tracker"

  execution_role_arn = module.ecs_task_role.arn
  cpu                = 1024
  memory             = 2048
  network_mode       = local.communication_tracker_task_network_mode
  region             = var.region
  task_role_arn      = module.ecs_task_role.arn


  containers = [
    {
      name         = "${local.stack_name}-communication_tracker"
      image_url    = "nginx"
      image_tag    = "latest"
      envs         = local.communication_tracker_envs
      ports        = [local.communication_tracker_port]
      command      = []
      memory       = 400
      sharedMemory = null
      volumes      = []
    },
  ]

  tags = local.common_tags
}

module "communication_tracker" {
  source = "../ecs-service"

  name = "${local.stack_name}-communication_tracker"

  vpc_id        = module.networking.vpc_id
  ecs_cluster   = module.ecs_cluster.name
  region        = var.region
  desired_count = var.ecs_desired_counts.communication_tracker_ecs_tasks
  network_mode  = local.communication_tracker_task_network_mode

  ecs_task_definition_arn = module.communication_tracker_ecs_task_definition.task_definition_arn
  subnet_ids              = module.networking.private_subnets
  assign_public_ip        = true

    alb_config = {
      domains                = ["/shubham"]
      listener_arn           = module.alb.listener_arn
      healthcheck_path       = "/"
      healthcheck_matcher    = "200"
      security_group_id      = module.alb.security_group_id
    # autoscaling_group_name = module.ecs_cluster.http_asg_name
      port                   = local.communication_tracker_port
    }

  tags = local.common_tags
}

