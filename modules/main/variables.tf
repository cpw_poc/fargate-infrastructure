variable "project_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "db_username" {
  type = string
}

variable "db_password" {
  type = string
}

variable "region" {
  type = string
}


variable "ecs_desired_counts" {
  type = object({
    communication_tracker_ecs_tasks = number
  })
}


