locals {
  stack_name = "${var.project_name}-${var.environment}"

   common_tags = {
    Project     = var.project_name
    Environment = var.environment
    TfModule    = "main"
  }
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.0.0"
    }
  }
}


