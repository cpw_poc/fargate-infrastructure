output "name" {
  value = var.name
}

output "security_group_id" {
  # may be null if service uses autoscaling group as target for load balancer
  value = length(aws_security_group.this) > 0 ? aws_security_group.this[0].id : null
}

output "service_arn" {
  value = aws_ecs_service.this.id
}

output "service_domain" {
  value = length(aws_service_discovery_service.this) > 0 ? aws_service_discovery_service.this[0].name : null
}

output "arn_suffix" {
  value = length(aws_alb_target_group.this) > 0 ? aws_alb_target_group.this[0].arn_suffix : null
}
