locals {
  tags = merge(var.tags, {
    TfModule = "ecs-service",
  })

  create_target_group = var.alb_config == null ? 0 : 1
}

resource "aws_security_group" "this" {
  count = var.network_mode == "awsvpc" ? 1 : 0

  name_prefix = "${var.name}-"
  vpc_id      = var.vpc_id

  egress {
    from_port   = 0
    protocol    = -1
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  dynamic "ingress" {
    for_each = var.allow_security_groups
    content {
      from_port       = ingress.value.port
      to_port         = ingress.value.port
      protocol        = "tcp"
      security_groups = ingress.value.group_ids
    }
  }

  dynamic "ingress" {
    for_each = var.alb_config == null ? [] : [1]
    content {
      from_port       = var.alb_config.port
      to_port         = var.alb_config.port
      protocol        = "tcp"
      security_groups = [var.alb_config.security_group_id]
    }
  }

  tags = local.tags

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_alb_target_group" "this" {
  count = local.create_target_group

  target_type = var.network_mode == "awsvpc" ? "ip" : "instance"

  protocol = "HTTP"
  vpc_id   = var.vpc_id
  port     = var.alb_config.port

  health_check {
    path                = var.alb_config.healthcheck_path
    matcher             = var.alb_config.healthcheck_matcher
    interval            = 5
    timeout             = 3
    healthy_threshold   = 2
    unhealthy_threshold = 3
  }

  # Name in tags because otherwise cannot create it before destroy
  tags = merge(local.tags, {
    Name = substr(var.name, 0, 32),
  })

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_alb_listener_rule" "this" {
  count = local.create_target_group

  listener_arn = var.alb_config.listener_arn

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.this[count.index].arn
  }

  condition {
    path_pattern {
      values = var.alb_config.domains
    }
  }

}



resource "aws_service_discovery_service" "this" {
  count = var.service_registry != null ? 1 : 0

  name = var.name

  dns_config {
    namespace_id = var.service_registry.namespace_id
    dns_records {
      ttl  = 30
      type = "A"
    }
  }

  tags = local.tags
}

resource "aws_ecs_service" "this" {
  name                = var.name
  cluster             = var.ecs_cluster
  launch_type         = "FARGATE"
  task_definition     = var.ecs_task_definition_arn
  desired_count       = var.desired_count
  scheduling_strategy = var.scheduling_strategy


  deployment_minimum_healthy_percent = var.desired_count == 1 ? 0 : 50

  dynamic "network_configuration" {
    for_each = aws_security_group.this
    content {
      security_groups = [aws_security_group.this[network_configuration.key].id]
      subnets         = var.subnet_ids
      assign_public_ip = var.assign_public_ip
    }
  }

  dynamic "service_registries" {
    for_each = aws_service_discovery_service.this
    content {
      registry_arn   = service_registries.value.arn
      container_port = var.network_mode == "awsvpc" ? 0 : var.service_registry.container_port
      container_name = var.service_registry.container_name
    }
  }

  dynamic "load_balancer" {
    for_each = length(aws_alb_target_group.this) > 0 && var.network_mode == "awsvpc" ? aws_alb_target_group.this : []
    content {
      target_group_arn = aws_alb_target_group.this[load_balancer.key].arn
      container_name   = var.name
      container_port   = var.alb_config.port
    }
  }

  tags = local.tags
}
