variable "name" {
  type = string
}

variable "vpc_id" {
  description = "Set only when using network mode awsvpc or host"
  type        = string
  default     = ""
}

variable "ecs_cluster" {
  type = string
}

variable "region" {
  type = string
}

variable "desired_count" {
  default = 1
}

variable "network_mode" {
  default = ""
}

variable "scheduling_strategy" {
  default = "REPLICA"
}

variable "subnet_ids" {
  description = "Used only when network mode is awsvpc"
  type        = list(string)
  default     = []
}

variable "assign_public_ip" {
  type= bool
  default = false
}

variable "ecs_task_definition_arn" {
  type = string
}

variable "alb_config" {
  type = object({
    domains             = list(string)
    port                = number
    listener_arn        = string
    healthcheck_path    = string
    healthcheck_matcher = string
    security_group_id   = string
    # Set only when using network mode host.
    # autoscaling_group_name = string
  })

  default = null
}

variable "allow_security_groups" {
  type = list(object({
    port      = number
    group_ids = list(string)
  }))

  default = []
}

variable "service_registry" {
  type = object({
    container_port = number
    container_name = string
    namespace_id   = string
  })
  default = null
}

variable "tags" {
  type = map(string)
}
