variable "name" {}

variable "s3_bucket_name" {}

variable "tags" {
  type = map(string)
}
