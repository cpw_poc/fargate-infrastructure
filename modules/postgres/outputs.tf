output "url" {
  value = "${var.postgis_config.enabled ? "postgis" : "postgres"}://${aws_db_instance.this_encrypted.username}:${aws_db_instance.this_encrypted.password}@${aws_db_instance.this_encrypted.endpoint}/${aws_db_instance.this_encrypted.db_name}"
}

output "subnet_group_arn" {
  value = aws_db_subnet_group.this.arn
}

output "db_arn" {
  value = aws_db_instance.this_encrypted.arn
}

output "identifier" {
  value = aws_db_instance.this_encrypted.identifier
}
