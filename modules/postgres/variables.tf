variable "name" {}

variable "apply_immediately" {}

variable "vpc_id" {}

variable "az" {}

variable "subnets" {
  type = list(object({
    cidr = string,
    az   = string,
  }))
}

variable "username" {}

variable "password" {}

variable "engine_version" {}

variable "parameter_group_family" {}

variable "multi_az" {}

variable "ingress_allow_security_groups" {
  type = list(string)
}

variable "instance_class" {}

variable "storage_type" {
  default = "gp2"
}

variable "allocated_storage" {}

variable "storage_encrypted" {
  default = false
}

variable "common_tags" {
  type = map(string)
}

variable "postgis_config" {
  type = object({
    enabled = bool
    cluster = string,
    region  = string,
  })

  default = {
    enabled = false,
    cluster = "",
    region  = "",
  }
}

# variable "lpm_vpc_peering_route" {
#   type = object({
#     target      = string
#     destination = string
#   })
# }

variable "subnet_ids"{
  type =any
}

variable "deletion_protection" {
  type=bool
}

