locals {
  port    = "5432"
  db_name = replace(var.name, "-", "")

  tags = merge(var.common_tags, {
    TfModule = "postgres"
  })
}

resource "aws_security_group" "this" {
  name_prefix = "${var.name}-postgres-"
  vpc_id      = var.vpc_id

   ingress {
     from_port       = local.port
     to_port         = local.port
     protocol        = "TCP"
     security_groups = compact(var.ingress_allow_security_groups)
   }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.tags

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group_rule" "this" {
  type              = "ingress"
  from_port         = 0
  to_port           = 5432
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]#[var.lpm_vpc_peering_route.destination]
  security_group_id = aws_security_group.this.id
}

# resource "aws_subnet" "this" {
#   count = length(var.subnets)

#   vpc_id            = var.vpc_id
#   cidr_block        = var.subnets[count.index].cidr
#   availability_zone = var.subnets[count.index].az

#   tags = merge(local.tags, {
#     Name = "${var.name}-postgres-${count.index}"
#   })
# }

resource "aws_db_subnet_group" "this" {
  name       = "${local.db_name}-group"
  subnet_ids = var.subnet_ids #aws_subnet.this.*.id

  tags = local.tags
}

# resource "aws_route_table" "this" {
#   vpc_id = var.vpc_id

#   tags = local.tags
# }

# resource "aws_route" "this" {
#   route_table_id            = aws_route_table.this.id
#   destination_cidr_block    = var.lpm_vpc_peering_route.destination
#   vpc_peering_connection_id = var.lpm_vpc_peering_route.target
# }

# resource "aws_route_table_association" "this" {
#   count = length(aws_subnet.this)

#   route_table_id = aws_route_table.this.id
#   subnet_id      = aws_subnet.this[count.index].id
# }

resource "aws_db_instance" "this_encrypted" {
  db_name = "${local.db_name}db"

  identifier = "${local.db_name}db"

  username = var.username
  password = var.password
  port     = local.port

  allocated_storage = var.allocated_storage
  instance_class    = var.instance_class

  engine         = "postgres"
  engine_version = var.engine_version

  vpc_security_group_ids = [aws_security_group.this.id]
  db_subnet_group_name   = aws_db_subnet_group.this.id
  #availability_zone      = var.az

  multi_az = var.multi_az

  maintenance_window          = "Mon:00:00-Mon:03:00"
  allow_major_version_upgrade = false
  auto_minor_version_upgrade  = false
  skip_final_snapshot         = false
  final_snapshot_identifier   = "${var.name}final"
  copy_tags_to_snapshot       = true
  storage_encrypted           = true
  storage_type                = var.storage_type
max_allocated_storage	= 100
  apply_immediately = true

  backup_window           = "10:20-10:50"
  backup_retention_period = 30
  deletion_protection      = var.deletion_protection
  # tags = local.tags

  tags = merge(local.tags, {
  RDS = "Auto-Shutdown"
  })

}
